package ru.tagirov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.IRepository;
import ru.tagirov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@RequiredArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> map = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull final T t) {
        map.put(t.getId(), t);
    }

    @Nullable
    @Override
    public T merge(@NotNull final T t) {
        return map.put(t.getId(), t);
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String s) {
        return map.get(s);
    }

    @NotNull
    @Override
    public Collection<T> findAll() {
        return map.values();
    }

    @Nullable
    @Override
    public T remove(@NotNull final String s) {
        return map.remove(s);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
