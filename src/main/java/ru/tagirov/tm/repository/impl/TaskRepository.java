package ru.tagirov.tm.repository.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.ITaskRepository;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.repository.AbstractRepository;
import ru.tagirov.tm.repository.PurposeRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class TaskRepository extends PurposeRepository<Task> implements ITaskRepository<Task> {

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull String userId, @Nullable final String projectId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : map.values()) {
            if (task.getUserId().equals(userId)) {
                if (task.getIdProject() != null) {
                    if (task.getIdProject().equals(projectId))
                        tasks.add(task);
                } else if (Objects.equals(task.getIdProject(), projectId))
                    tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final List<Task> taskList = new ArrayList<>(map.values());
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(userId)) {
                if (task.getIdProject() != null) {
                    if (task.getIdProject().equals(projectId))
                        remove(task.getId());
                } else if (Objects.equals(task.getIdProject(), projectId))
                    remove(task.getId());
            }
        }
    }

}
