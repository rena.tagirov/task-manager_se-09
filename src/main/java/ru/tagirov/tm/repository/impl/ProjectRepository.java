package ru.tagirov.tm.repository.impl;

import lombok.RequiredArgsConstructor;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.repository.AbstractRepository;
import ru.tagirov.tm.api.repository.IProjectRepository;
import ru.tagirov.tm.repository.PurposeRepository;

@RequiredArgsConstructor
public class ProjectRepository extends PurposeRepository<Project> implements IProjectRepository<Project> {

}
