package ru.tagirov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.IPurposeRepository;
import ru.tagirov.tm.entity.PurposeEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public abstract class PurposeRepository<T extends PurposeEntity> extends AbstractRepository<T> implements IPurposeRepository<T> {

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String uuid) {
        @NotNull T t = map.get(uuid);
        if (t.getUserId().equals(userId))
            return map.get(uuid);
        return null;
    }

    @NotNull
    public Collection<T> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<T> tList = new ArrayList<>();
        for (@NotNull final T t : map.values()) {
            if (t.getUserId().equals(userId))
                tList.add(t);
        }
        return tList;
    }

    @Nullable
    public T remove(@NotNull final String userId, @NotNull final String uuid) {
        @NotNull final T t = map.get(uuid);
        if (t.getUserId().equals(userId))
            return map.remove(uuid);
        return null;
    }

    public void removeAll(@NotNull final String userId) {
        @NotNull final List<T> tList = new ArrayList<>(map.values());
        for (@NotNull final T t : tList) {
            if (t.getUserId().equals(userId))
                map.remove(t.getId());
        }
    }


    @Override
    public @NotNull Collection<T> findAllByLine(@NotNull String userId, @NotNull String line) {
        @NotNull final List<T> tList = new ArrayList<>();
        for (@NotNull final T t : map.values()) {
            if (t.getUserId().equals(userId))
                if (t.getName().contains(line))
                    tList.add(t);
                else if (t.getDescription().contains(line))
                    tList.add(t);
        }
        return tList;
    }

}
