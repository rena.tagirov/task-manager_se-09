package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public interface IPurposeService<T extends AbstractEntity> extends IService<T> {

    @Override
    void persist(@NotNull final T t);

    @Nullable
    @Override
    T merge(@NotNull final T t);

    @Nullable
    @Override
    T findOne(@NotNull final T t);

    @NotNull
    @Override
    Collection<T> findAll();

    @Nullable
    @Override
    T remove(@NotNull final T t);

    @Override
    void removeAll();

    @Nullable
    T findOne(@NotNull final User user, @NotNull final T t);

    @NotNull
    Collection<T> findAllByUserId(@NotNull final String userId);

    @Nullable
    T remove(@NotNull final User user, @NotNull final T t);

    void removeAll(@NotNull final String userId);

    @NotNull
    Collection<T> findAllByLine(@NotNull final String userId, @NotNull final String line);

}
