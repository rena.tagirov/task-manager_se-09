package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public interface ITaskService<T extends AbstractEntity> extends IPurposeService<T> {

    @Override
    void persist(@NotNull final T t);

    @Nullable
    @Override
    T merge(@NotNull final T t);

    @Nullable
    @Override
    T findOne(@NotNull final T t);

    @Override
    @NotNull Collection<T> findAll();

    @Nullable
    @Override
    T remove(@NotNull final T t);

    @Override
    void removeAll();

    @Nullable
    @Override
    T findOne(final @NotNull User user, @NotNull final T t);

    @Override
    @NotNull
    Collection<T> findAllByUserId(final @NotNull String userId);

    @Nullable
    @Override
    T remove(final @NotNull User user, @NotNull final T t);

    @NotNull
    @Override
    Collection<T> findAllByLine(final @NotNull String userId, final @NotNull String line);

    @Override
    void removeAll(final @NotNull String userId);

    @NotNull
    Collection<T> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId);

    void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId);

}
