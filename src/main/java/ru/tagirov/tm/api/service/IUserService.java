package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public interface IUserService<T extends AbstractEntity> extends IService<T>{

    @Override
    void persist(@NotNull final T t);

    @Nullable
    @Override
    T merge(@NotNull final T t);

    @Nullable
    @Override
    T findOne(@NotNull final T t);

    @Override
    @NotNull Collection<T> findAll();

    @Nullable
    @Override
    T remove(@NotNull final T t);

    @Override
    void removeAll();

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @Nullable
    User findOneByLogin(@NotNull final String login);

}