package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public interface IUserRepository<T extends AbstractEntity> extends IRepository<T> {

    @Override
    void persist(@NotNull final T t);

    @Nullable
    @Override
    T merge(@NotNull final T t);

    @Nullable
    @Override
    T findOne(@NotNull final String uuid);

    @NotNull
    @Override
    Collection<T> findAll();

    @Nullable
    @Override
    T remove(@NotNull final String uuid);

    @Override
    void removeAll();

    @Nullable User findOneByLogin(@NotNull final String login);

}
