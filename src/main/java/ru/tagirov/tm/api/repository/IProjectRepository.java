package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IProjectRepository<T extends AbstractEntity> extends IPurposeRepository<T> {

    @Override
    void persist(@NotNull final T t);

    @Nullable
    @Override
    T merge(@NotNull final T t);

    @Nullable
    @Override
    T findOne(@NotNull final String uuid);

    @NotNull
    @Override
    Collection<T> findAll();

    @Nullable
    @Override
    T remove(@NotNull final String uuid);

    @Override
    void removeAll();

    @Nullable
    @Override
    T findOne(@NotNull final String userId, @NotNull final String uuid);

    @NotNull
    @Override
    Collection<T> findAllByUserId(@NotNull final String userId);

    @Nullable
    @Override
    T remove(@NotNull final String userId, @NotNull final String uuid);

    @Override
    void removeAll(@NotNull final String userId);

    @NotNull
    @Override
    Collection<T> findAllByLine(final @NotNull String userId, final @NotNull String line);
}
