package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskSearchCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "task-find";
    }

    @Override
    public @NotNull String description() {
        return "Find task by name or description.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[TASK FIND]");
        System.out.println("Find:");
        @NotNull final String line = serviceLocator.getTerminalService().readLine();

        @NotNull final List<Task> findTasks = new ArrayList<>(taskService.findAllByLine(user.getId(), line));

        serviceLocator.getTerminalService().printTasks(findTasks);
    }

}
