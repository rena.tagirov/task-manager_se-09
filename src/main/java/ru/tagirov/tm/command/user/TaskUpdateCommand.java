package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.terminal.TerminalService;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "task update";
    }

    @Override
    public @NonNull String description() {
        return "update one task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, ParseException {

        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        @NotNull final List<Task> tasks = new ArrayList<>(taskService.findAllByUserId(user.getId()));

        System.out.println("[TASK UPDATE]");
        serviceLocator.getTerminalService().printTasks(tasks);

        System.out.println("ENTER NUMBER:");
        final int number = serviceLocator.getTerminalService().parseInt();

        if (number <= 0 || number > tasks.size()) {
            System.out.println("Enter correct number!");
            return;
        }

        for (int i = 0; i < tasks.size(); i++) {
            if (number - 1 == i) {
                taskService.merge(selectField(tasks.get(i)));
                break;
            }
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Task selectField(@NotNull final Task task) throws ParseException, IOException {
        serviceLocator.getTerminalService().printTaskField();

        @NotNull final String fieldNumber = serviceLocator.getTerminalService().readLine();
        switch (fieldNumber) {
            case "1":
                System.out.println("Replace name to:");
                task.setName(serviceLocator.getTerminalService().readLine());
                break;
            case "2":
                System.out.println("Replace description to:");
                task.setDescription(serviceLocator.getTerminalService().readLine());
                break;
            case "3":
                System.out.println("Replace start date to:");
                task.setDateCreate(DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            case "4":
                System.out.println("Replace end date to:");
                task.setDateUpdate(DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            case "6":
                System.out.println("Replace project to(number):");
                @Nullable final String projectId = getProjectId(task.getIdProject());
                task.setIdProject(projectId);
                break;
            default:
                System.out.println("Nothing has changed!");
                break;
        }

        return task;
    }

    @Nullable
    private String getProjectId(@Nullable final String projectId) throws IOException {
        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));
        serviceLocator.getTerminalService().printProjects(projects);

        final int number = serviceLocator.getTerminalService().parseInt();

        if (number == 0) {
            System.out.println("NON-PROJECT");
            return null;
        }

        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return projectId;
        }

        @Nullable Project project = null;
        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i)
                project = projects.get(i);
        }

        return Objects.requireNonNull(project).getId();
    }
}
