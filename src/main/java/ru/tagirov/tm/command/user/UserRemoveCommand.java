package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.util.Objects;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user remove";
    }

    @NotNull
    @Override
    public String description() {
        return "remove your account";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @NotNull
    @Override
    public Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() {
        @NotNull final IUserService<User> userService = serviceLocator.getIUserService();
        @NotNull final IProjectService<Project> projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService<Task> taskService = serviceLocator.getITaskService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[USER REMOVE]");
        projectService.removeAll(user.getId());
        taskService.removeAll(user.getId());
        userService.remove(user);
        serviceLocator.getIUserService().setCurrentUser(null);
        System.out.println("[OK]");
    }
}
