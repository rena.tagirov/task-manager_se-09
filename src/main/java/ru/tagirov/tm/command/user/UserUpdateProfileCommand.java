package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.terminal.TerminalService;

import java.io.IOException;
import java.util.Objects;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "user update";
    }

    @Override
    public @NonNull String description() {
        return "user update";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {

        @NotNull final IUserService<User> userService = serviceLocator.getIUserService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().readLine();

        if (userService.findOneByLogin(login) != null) {
            System.out.println("LOGIN IS EXIST!");
            return;
        }

        user.setLogin(login);
        userService.merge(user);
        System.out.println("[OK]");
    }
}

