package ru.tagirov.tm.command.admin;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserListCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "user list";
    }

    @Override
    public @NonNull String description() {
        return "show all users";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.ADMIN;
    }

    @Override
    public void execute() throws IOException {

        @NotNull final IUserService<User> userService = serviceLocator.getIUserService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        if (user.getRole() == Role.USER) {
            System.out.println("No access");
            return;
        }
        @NotNull final List<User> users = new ArrayList<>(userService.findAll());

        for (User user1 : users) {
            serviceLocator.getTerminalService().printUser(user1);
        }
    }
}
