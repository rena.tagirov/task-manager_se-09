package ru.tagirov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.PurposeStatus;

public final class StatusUtil {

    @NotNull
    public static PurposeStatus changeStatus(@NotNull final String line, @NotNull PurposeStatus purposeStatus) {
        switch (line) {
            case "1":
                purposeStatus = PurposeStatus.PLANNED;
                break;
            case "2":
                purposeStatus = PurposeStatus.DURING;
                break;
            case "3":
                purposeStatus = PurposeStatus.READY;
                break;
            default:
                System.out.println("Status don't changed!");
        }

        return purposeStatus;
    }
}
