package ru.tagirov.tm;

import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.util.Md5Util;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.UUID;

public class TestClass {
    public ServiceLocator serviceLocator;

    public TestClass(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    public void test() throws NoSuchAlgorithmException, ParseException {
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"a", Md5Util.getHash("a"), Role.ADMIN));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"m", Md5Util.getHash("m"), Role.MANAGER));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"zakir", Md5Util.getHash("12345"), Role.USER));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"timur", Md5Util.getHash("23456"), Role.USER));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"azat", Md5Util.getHash("34567"), Role.USER));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"aidar", Md5Util.getHash("45678"), Role.USER));
        serviceLocator.getIUserService().persist(new User(UUID.randomUUID().toString(),"airat", Md5Util.getHash("56789"), Role.USER));

        for (User tmp : serviceLocator.getIUserService().findAll()){
            if(!(tmp.getRole() == Role.ADMIN && tmp.getRole() == Role.MANAGER)) {
                serviceLocator.getIProjectService().persist(new Project(UUID.randomUUID().toString(), "This project is " + tmp.getLogin() + "." + " Project №1", "description1", DateUtil.getDate("11-11-2021"),DateUtil.getDate("11-12-2021"), tmp.getId()));
                serviceLocator.getIProjectService().persist(new Project(UUID.randomUUID().toString(), "This project is " + tmp.getLogin() + "." + " Project №2", "description2", DateUtil.getDate("11-11-2021"),DateUtil.getDate("11-12-2021"), tmp.getId()));
                serviceLocator.getIProjectService().persist(new Project(UUID.randomUUID().toString(), "This project is " + tmp.getLogin() + "." + " Project №3", "description3", DateUtil.getDate("11-11-2021"),DateUtil.getDate("11-12-2021"), tmp.getId()));
//                serviceLocator.getITaskService().persist(new Task(UUID.randomUUID().toString(), "This task is " + tmp.getLogin() + "." + " Task №1", "description1", DateUtil.getDate("11-11-2021"),DateUtil.getDate("11-12-2021"), tmp.getId()));
//                serviceLocator.getITaskService().persist(new Task(UUID.randomUUID().toString(), "This task is " + tmp.getLogin() + "." + " Task №2", "description1", DateUtil.getDate("11-11-2021"),DateUtil.getDate("11-12-2021"), tmp.getId()));
            }

        }
    }
}