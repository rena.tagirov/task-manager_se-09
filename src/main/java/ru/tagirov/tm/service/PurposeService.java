package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.IPurposeRepository;
import ru.tagirov.tm.api.service.IPurposeService;
import ru.tagirov.tm.entity.PurposeEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public abstract class PurposeService<T extends PurposeEntity> extends AbstractService<T> implements IPurposeService<T> {

    @NotNull
    private IPurposeRepository<T> purposeRepository;

    public PurposeService() {
    }

    public PurposeService(@NotNull final IPurposeRepository<T> purposeRepository) {
        super(purposeRepository);
        this.purposeRepository = purposeRepository;
    }

    @Nullable
    public T findOne(@NotNull final User user, @NotNull final T t) {
        return purposeRepository.findOne(user.getId(), t.getId());
    }

    @NotNull
    public Collection<T> findAllByUserId(@NotNull final String userId) {
        return purposeRepository.findAllByUserId(userId);
    }

    @Nullable
    public T remove(@NotNull final User user, @NotNull final T t) {
        return purposeRepository.remove(user.getId(), t.getId());
    }

    public void removeAll(@NotNull final String userId) {
        purposeRepository.removeAll(userId);
    }

    @Override
    public @NotNull Collection<T> findAllByLine(@NotNull String userId, @NotNull String line) {
        return purposeRepository.findAllByLine(userId, line);
    }
}
