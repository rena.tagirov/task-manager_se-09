package ru.tagirov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.ITaskRepository;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.service.AbstractService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.service.PurposeService;

import java.util.Collection;

public class TaskService extends PurposeService<Task> implements ITaskService<Task> {

    @NotNull
    private ITaskRepository<Task> taskRepository;

    public TaskService(@NotNull final ITaskRepository<Task> taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable String projectId) {
        taskRepository.removeAllByProjectId(userId, projectId);
    }

}
