package ru.tagirov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.service.IService;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.api.repository.IRepository;

import java.util.Collection;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private IRepository<T> abstractRepository;

    public AbstractService() {
    }

    public AbstractService(@NotNull IRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public void persist(@NotNull final T t) {
        abstractRepository.persist(t);
    }

    @Nullable
    @Override
    public T merge(@NotNull final T t) {
        return abstractRepository.merge(t);
    }

    @Nullable
    @Override
    public T findOne(@NotNull final T t) {
        return abstractRepository.findOne(t.getId());
    }

    @Nullable
    @Override
    public T remove(@NotNull final T t) {
        return abstractRepository.remove(t.getId());
    }

    @NotNull
    @Override
    public Collection<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }

}